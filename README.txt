Feeds User Roles
================

This module adds per-role "has this role" Feeds mapping targets for each role in
the system. This allows you to create a single Feeds importer for all your users
rather than one importer per-role.

Quick Start
-----------

1. Install the module.

2. Use the "Role: X" targets in your user importer mappings.

3. ???

4. Profit.

Details
-------

1. Download and unpack Feeds User Roles in sites/all/modules (or, if you prefer,
   some other modules directory). This module requires [Feeds][1], so you'll
   need to install it too.

2. Define some custom roles at admin/people/permissions/roles. For the sake of
   an example, create a role called administrator.

3. Create or edit a Feeds Importer at admin/structure/feeds using the User
   processor. For the sake of an example, call the importer User.

4. Edit the Mapping for your importer. The mappers dropdown will include an
   option for "Role: administrator". If the source data you assign to the "Role:
   administrator" target looks like [true according to PHP][2] then the imported
   user will be granted the "administrator" role, if it looks false according to
   PHP then the imported user *will not* be granted the role.

[1]: http://drupal.org/project/feeds
[2]: http://php.net/bool#language.types.boolean.casting
